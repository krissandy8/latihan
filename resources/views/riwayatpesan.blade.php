@extends('layouts.layout')

@section('navbar')
@include('layouts.navbarsolid')
@endsection

@section('content')
<section class="module">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div>
                <div class="col-sm-6"><h4 class="font-alt mb-0">Riwayat Pemesanan</h4> </div>
                <div class="col-sm-6"><h4 style="color:#337ab7;">No Pemesanan : 1-2048-8103741</h4></div>
            </div>
          
          <hr class="divider-w mt-10 mb-20">
          <table class="table table-striped table-border checkout-table">
            <tbody>
              <tr>
                <th class="hidden-xs">Item</th>
                <th>Description</th>
                <th class="hidden-xs">Price</th>
                <th>Qty</th>
                <th>Total</th>
              </tr>
              <tr>
                <td class="hidden-xs"><a href="#"><img src="assets/images/shop/MEGX570GODLIKE.png" alt="MEG X570 GODLIKE"/></a></td>
                <td>
                  <h5 class="product-title font-alt">MSI MEG X570 GODLIKE</h5>
                </td>
                <td class="hidden-xs">
                  <h5 class="product-title font-alt">RP9.851.000</h5>
                </td>
                <td>
                    <h5 class="product-title font-alt">1</h5>
                </td>
                <td>
                  <h5 class="product-title font-alt">RP9.851.000</h5>
                </td>
              </tr>
              <tr>
                <td class="hidden-xs"><a href="#"><img src="assets/images/shop/MEGX570UNIFY.png" alt="MEG X570 UNIFY"/></a></td>
                <td>
                  <h5 class="product-title font-alt">MSI MEG X570 UNIFY</h5>
                </td>
                <td class="hidden-xs">
                  <h5 class="product-title font-alt">Rp5.728.000</h5>
                </td>
                <td>
                    <h5 class="product-title font-alt">1</h5>
                </td>
                <td>
                  <h5 class="product-title font-alt">Rp5.728.000</h5>
                </td>
              </tr>
            </tbody>
          </table>
          <div>
            <div class="col-sm-12"><h4 class="font-alt mb-0">Status Pemesanan</h4> </div>
        </div>

        <div class="col" style="text-align: center;">
                <div class="col-sm-3">
                    <button type="button" class="btn btn-g btn-circle" disabled="disabled">1</button>
                    <p>Cart</p>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-circle">2</button>
                    <p>Pembayaran</p>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-g btn-circle" disabled="disabled">3</button>
                    <p>Konfirmasi</p>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-g btn-circle" disabled="disabled">4</button>
                    <p>Pengiriman</p>
                </div> 

        </div>
          <hr class="divider-w mt-10 mb-20">
          <blockquote style="text-align: center;">
            <p>Terima kasih telah melakukan pemesanan, saat ini dalam proses konfirmasi admin</p>
            <button onclick="window.location.href='/home';" class="btn btn-lg btn-round btn-b" type="submit"><i class="fas fa-home"></i> &nbsp;Kembali ke Home</button>
          </blockquote>
        </div>
      </div>
    </div>
  </section>
@endsection