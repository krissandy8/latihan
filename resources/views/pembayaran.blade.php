@extends('layouts.layout')

@section('content')
<section class="module">
  <div class="container">
    <div class="row mb-60">
      <div class="col-sm-8 col-sm-offset-2">
         <h2>Pembayaran otomatis melalui Virtual Account</h2>
         <h4 style="color:red;">No Pemesanan : 1-2048-8103741</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <div role="tabpanel">
          <ul class="nav nav-tabs font-alt" role="tablist">
            <li class="active"><a href="#bca" data-toggle="tab">BCA</a></li>
            <li><a href="#mandiri" data-toggle="tab">Mandiri</a></li>
            <li><a href="#bni" data-toggle="tab">BNI</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="bca">
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#iBanking">iBanking</a></h4>
                  </div>
                  <div class="panel-collapse collapse in" id="iBanking">
                    <div class="panel-body">
                      <ol>
                        <li>Pilih Transfer Dana > Transfer ke BCA Virtual Account.</li>
                        <li>Centang no. Virtual Account dan masukkan 333 08XXXXXXXX (3 kode bank + no. yang tertera di halaman Pembayaran) dan klik Lanjutkan.</li>
                        <li>Masukkan nominal pembayaran sesuai pesanan</li>
                        <li>Periksa informasi yang tertera di layar. Pastikan nomor pemesanan sesuai. Jika benar, klik Lanjutkan.</li>
                        <li>Masukkan respon KeyBCA Anda dan klik Kirim.</li>
                      </ol> 
                      </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#mBanking">mBanking</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="mBanking">
                    <div class="panel-body">
                      <li>Pilih Transfer Dana > Transfer ke BCA Virtual Account.</li>
                      <li>Masukkan no. Virtual Account 333 08XXXXXXXX (3 kode bank + no. yang tertera di halaman Pembayaran) dan pilih Send.</li>
                      <li>Masukkan nominal pembayaran sesuai pesanan</li>
                      <li>Periksa informasi yang tertera di layar. Pastikan nomor pemesanan sesuai. Jika benar, pilih OK</li>
                      <li>Masukkan PIN m-BCA Anda dan pilih OK.</li>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#atm">ATM</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="atm">
                    <div class="panel-body">
                      <li>Pilih Transaksi Lainnya > Pilih Transfer > Pilih Ke Rek BCA Virtual Account.</li>
                      <li>Masukkan no. Virtual Account 333 08XXXXXXXX (3 kode bank + no. yang tertera di halaman Pembayaran) dan pilih Benar.</li>
                      <li>Masukkan nominal pembayaran sesuai pesanan</li>
                      <li>Periksa informasi yang tertera di layar. Pastikan nomor pemesanan sesuai. Jika benar, pilih YA</li>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="mandiri">
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#miBanking">iBanking</a></h4>
                  </div>
                  <div class="panel-collapse collapse in" id="miBanking">
                    <div class="panel-body">
                      <ol>
                        <li>Pilih Bayar/Beli.</li>
                        <li>Pilih Lainnya > Lainnya > Multi Payment.</li>
                        <li>Masukkan no. Virtual Account 893 08XXXXXXXX (3 kode bank + no. yang tertera di halaman Pembayaran) lalu klik Lanjutkan.</li>
                        <li>Masukkan nominal pembayaran sesuai pesanan</li>
                        <li>Periksa informasi yang tertera di layar. Total tagihan sudah benar dan username Anda {username}. Jika benar, centang tagihan dan klik Lanjutkan.</li>
                        <li>Masukkan PIN Token Anda dan klik Kirim.</li>
                      </ol> 
                      </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#mmBanking">mBanking</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="mmBanking">
                    <div class="panel-body">
                      <li>Login ke mBanking Anda. Pilih Bayar, kemudian pilih Multi Payment.</li>
                      <li>Pilih Penyedia Layanan: Comskit, dan masukkan no. Virtual Account 893 08XXXXXXXX (3 kode bank + no. yang tertera di halaman Pembayaran), kemudian pilih Lanjut.</li>
                      <li>Masukkan nominal pembayaran sesuai pesanan</li>
                      <li>Periksa informasi yang tertera di layar. Total tagihan sudah benar dan username Anda {username}. Jika benar, centang tagihan dan klik Lanjutkan.</li>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#matm">ATM</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="matm">
                    <div class="panel-body">
                      <li>Pilih Bayar/Beli.</li>
                      <li>Pilih Lainnya > Lainnya > Multi Payment.</li>
                      <li>Masukkan kode perusahaan 89308 dan pilih Benar.</li>
                      <li>Masukkan no. Virtual Account  893 08XXXXXXXX  (3 digit kode bank + no. yang tertera di halaman Pembayaran) dan pilih Benar.</li>
                      <li>Masukkan nominal pembayaran sesuai pesanan</li>
                      <li>Periksa informasi yang tertera di layar. Total tagihan sudah benar dan username Anda {username}. Jika benar, centang tagihan dan klik Lanjutkan.</li>
                      <li>Periksa layar konfirmasi dan pilih Ya.</li>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="bni">
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#biBanking">iBanking</a></h4>
                  </div>
                  <div class="panel-collapse collapse in" id="biBanking">
                    <div class="panel-body">
                      <ol>
                        <li>Pilih Transfer > Virtual Account Billing.</li>
                        <li>Masukkan no. Virtual Account 807 08XXXXXXXX (3 digit kode bank + no. yang tertera di halaman Pembayaran).</li>
                        <li>Pilih Rekening Debet dan klik Lanjut.</li>
                        <li>Masukkan nominal pembayaran sesuai pesanan</li>
                        <li>Periksa informasi yang tertera di layar. Total tagihan sudah benar dan username Anda {username}. Jika benar, centang tagihan dan klik Lanjutkan.</li>
                        <li>Masukkan Kode Otentikasi Token Anda, lalu klik Proses</li>
                      </ol> 
                      </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#bmBanking">mBanking</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="bmBanking">
                    <div class="panel-body">
                      <li>ilih menu Transfer > Virtual Account Billing.</li>
                      <li>Pilih Rekening Debet > Masukkan kode bayar 8807 08XXXXXXXX (3 digit kode bank + no. yang tertera di halaman Pembayaran) pada menu Input Baru.</li>
                      <li>Masukkan nominal pembayaran sesuai pesanan</li>
                      <li>Periksa informasi yang tertera di layar. Total tagihan sudah benar dan username Anda {username}. Jika benar, centang tagihan dan klik Lanjutkan.</li>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#batm">ATM</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="batm">
                    <div class="panel-body">
                      <li>Pilih Menu Lain > Transfer.</li>
                      <li>Pilih jenis rekening asal dan pilih Virtual Account Billing.</li>
                      <li>Masukkan no. Virtual Account 8807 08XXXXXXXX (3 kode bank + no. yang tertera di halaman Pembayaran) dan pilih Benar.</li>
                      <li>Masukkan nominal pembayaran sesuai pesanan</li>
                      <li>Periksa informasi yang tertera di layar. Total tagihan sudah benar dan username Anda {username}. Jika benar, centang tagihan dan klik Lanjutkan.</li>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <button onclick="window.location.href='/riwayat';" class="btn btn-lg btn-block btn-round btn-danger" type="submit">Konfirmasi</button>
      </div>
    </div>
  </div>
</section>
@endsection