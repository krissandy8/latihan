@extends('layouts.layout')

@section('navbar')
@include('layouts.navbarsolid')
@endsection

@section('content')
<section class="module">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 mb-sm-40"><a class="gallery" href="assets/images/shop/MEGX570GODLIKE.png"><img src="assets/images/shop/MEGX570GODLIKE.png" alt="Single Product Image"/></a>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-12">
              <h1 class="product-title font-alt">MSI MEG X570 GODLIKE</h1>
            </div>
          </div>
          <div class="row mb-20">
            <div class="col-sm-12"><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span><a class="open-tab section-scroll" href="#reviews">-2customer reviews</a>
            </div>
          </div>
          <div class="row mb-20">
            <div class="col-sm-12">
              <div class="price font-alt"><span class="amount">Rp9.851.000</span></div>
            </div>
          </div>
          <div class="row mb-20">
            <div class="col-sm-12">
              <div class="description">
                <p>Supports AMD Ryzen™ 5000 Series/ 2nd and 3rd Gen AMD Ryzen™ / Ryzen™ 4000 G-Series / Ryzen™ with Radeon™ Vega Graphics and 2nd Gen AMD Ryzen™ with Radeon™ Graphics Desktop Processors for AM4 socket</p>
              </div>
            </div>
          </div>
          <div class="row mb-20">
            <div class="col-sm-4 mb-sm-20">
              <input class="form-control input-lg" type="number" name="" value="1" max="40" min="1" required="required"/>
            </div>
            <div class="col-sm-8"><a class="btn btn-lg btn-block btn-round btn-b" href="#">Add To Cart</a></div>
          </div>
          <div class="row mb-20">
            <div class="col-sm-12">
              <div class="product_meta">Categories:<a href="#"> Motherboard, </a><a href="#">Gaming, </a><a href="#">MSI</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-20">
        <div class="col-sm-12">
          <ul class="nav nav-tabs font-alt" role="tablist">
            <li class="active"><a href="#description" data-toggle="tab"><span class="icon-tools-2"></span>Description</a></li>
            <li><a href="#data-sheet" data-toggle="tab"><span class="icon-tools-2"></span>Specification</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="description">
              <p>MEG X570 GODLIKE embodies another culmination of cutting edge design, spectacular aesthetics, and heavy duty reliability from a long line of divine flagship motherboards by MSI GAMING. Beneath the deep spectacle of Mystic Light Infinity II lies a powerful Core Boost power delivery system capable of pushing overclocked AMD Ryzen processors to their limit. Get more than a motherboard with incredible expandability and the included accessory cards.</p>
              <p>Supports AMD Ryzen™ 5000 Series/ 2nd and 3rd Gen AMD Ryzen™ / Ryzen™ 4000 G-Series / Ryzen™ with Radeon™ Vega Graphics and 2nd Gen AMD Ryzen™ with Radeon™ Graphics Desktop Processors for AM4 socket</p>
              <p>Supports DDR4 Memory, up to 5000+(OC) MHz</p>
            </div>
            <div class="tab-pane" id="data-sheet">
              <table class="table table-striped ds-table table-responsive">
                <tbody>
                  <tr>
                    <th>Title</th>
                    <th>Info</th>
                  </tr>
                  <tr>
                    <td>SOCKET</td>
                    <td>AM4</td>
                  </tr>
                  <tr>
                    <td>CPU (MAX SUPPORT)</td>
                    <td>RYZEN 9</td>
                  </tr>
                  <tr>
                    <td>CHIPSET</td>
                    <td>AMD® X570 Chipset</td>
                  </tr>
                  <tr>
                    <td>DDR4 MEMORY</td>
                    <td>1866/ 2133/ 2400/ 2667/ 2800/2933 /3000 /3066 /3200 /3466 /3600 /3733 /3866 /4000 /4133 /4266 /4400 /4533 /4600 /4733 /4800/5000+ Mhz by JEDEC and A-XMP OC MODE</td>
                  </tr>
                  <tr>
                    <td>MEMORY CHANNEL</td>
                    <td>Dual</td>
                  </tr>
                  <tr>
                    <td>DIMM SLOTS</td>
                    <td>4</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection