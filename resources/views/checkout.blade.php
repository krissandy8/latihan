@extends('layouts.layout')

@section('content')
<section class="module">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
          <h1 class="module-title font-alt">Checkout</h1>
        </div>
      </div>
      {{-- <hr class="divider-w pt-10"> --}}
      <div class="row">
        <div class="col-sm-12">
          <table class="table table-striped table-border checkout-table">
            <tbody>
              <tr>
                <th class="hidden-xs">Item</th>
                <th>Description</th>
                <th class="hidden-xs">Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th>Remove</th>
              </tr>
              <tr>
                <td class="hidden-xs"><a href="#"><img src="assets/images/shop/MEGX570GODLIKE.png" alt="MEG X570 GODLIKE"/></a></td>
                <td>
                  <h5 class="product-title font-alt">MSI MEG X570 GODLIKE</h5>
                </td>
                <td class="hidden-xs">
                  <h5 class="product-title font-alt">RP9.851.000</h5>
                </td>
                <td>
                  <input class="form-control" type="number" name="" value="1" max="50" min="1"/>
                </td>
                <td>
                  <h5 class="product-title font-alt">RP9.851.000</h5>
                </td>
                <td class="pr-remove"><a href="#" title="Remove"><i class="fa fa-times"></i></a></td>
              </tr>
              <tr>
                <td class="hidden-xs"><a href="#"><img src="assets/images/shop/MEGX570UNIFY.png" alt="MEG X570 UNIFY"/></a></td>
                <td>
                  <h5 class="product-title font-alt">MSI MEG X570 UNIFY</h5>
                </td>
                <td class="hidden-xs">
                  <h5 class="product-title font-alt">Rp5.728.000</h5>
                </td>
                <td>
                  <input class="form-control" type="number" name="" value="1" max="50" min="1"/>
                </td>
                <td>
                  <h5 class="product-title font-alt">Rp5.728.000</h5>
                </td>
                <td class="pr-remove"><a href="#" title="Remove"><i class="fa fa-times"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <input class="form-control" type="text" id="" name="" placeholder="Coupon code"/>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <button class="btn btn-round btn-g" type="submit">Apply</button>
          </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
          <div class="form-group">
            <button class="btn btn-block btn-round btn-d pull-right" type="submit">Update Cart</button>
          </div>
        </div>
      </div>
      <hr class="divider-w">
      <div class="row mt-20">
        <div class="col-sm-5 col-sm-offset-7">
          <div class="shop-Cart-totalbox">
            <h4 class="font-alt">Cart Totals</h4>
            <table class="table table-striped table-border checkout-table">
              <tbody>
                <tr>
                  <th>Cart Subtotal :</th>
                  <td>Rp15.579.000</td>
                </tr>
                <tr>
                  <th>Shipping Total :</th>
                  <td>Rp50.000</td>
                </tr>
                <tr class="shop-Cart-totalprice">
                  <th>Total :</th>
                  <td>Rp15.629.000</td>
                </tr>
              </tbody>
            </table>
            <button onclick="window.location.href='/bayar';" class="btn btn-lg btn-block btn-round btn-d" type="submit">Proceed to Payment</button>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection