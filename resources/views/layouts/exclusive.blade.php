<section class="module">
    <div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
        <h2 class="module-title font-alt">Exclusive products</h2>
        </div>
    </div>
    <div class="row">
        <div class="owl-carousel text-center" data-items="5" data-pagination="false" data-navigation="false">
        <div class="owl-item">
            <div class="col-sm-12">
            <div class="ex-product"><a href="#"><img src="assets/images/shop/OptixMPG341CQR.png" alt="Optix MPG341CQR"/></a>
                <h4 class="shop-item-title font-alt"><a href="#">Optix MPG341CQR</a></h4>Rp17.218.000
            </div>
            </div>
        </div>
        <div class="owl-item">
            <div class="col-sm-12">
            <div class="ex-product"><a href="#"><img src="assets/images/shop/OptixMAG274QRF-QD.png" alt="Optix MAG274QRF-QD"/></a>
                <h4 class="shop-item-title font-alt"><a href="#">Optix MAG274QRF-QD</a></h4>Rp7.300.000
            </div>
            </div>
        </div>
        <div class="owl-item">
            <div class="col-sm-12">
            <div class="ex-product"><a href="#"><img src="assets/images/shop/MAGVAMPIRIC100R.png" alt="MAG VAMPIRIC 100R"/></a>
                <h4 class="shop-item-title font-alt"><a href="#">MAG VAMPIRIC 100R</a></h4>Rp650.000
            </div>
            </div>
        </div>
        <div class="owl-item">
            <div class="col-sm-12">
            <div class="ex-product"><a href="#"><img src="assets/images/shop/MEGTridentX10th.png" alt="MEG Trident X 10th"/></a>
                <h4 class="shop-item-title font-alt"><a href="#">MEG Trident X 10th</a></h4>Rp40.999.000
            </div>
            </div>
        </div>
        <div class="owl-item">
            <div class="col-sm-12">
            <div class="ex-product"><a href="#"><img src="assets/images/shop/MPGTrident3Arctic10th.png" alt="MPG Trident 3 Arctic 10th"/></a>
                <h4 class="shop-item-title font-alt"><a href="#">MPG Trident 3 Arctic 10th</a></h4>Rp16.999.000
            </div>
            </div>
        </div>
        <div class="owl-item">
            <div class="col-sm-12">
            <div class="ex-product"><a href="#"><img src="assets/images/shop/HS01HEADSETSTAND.png" alt="HS01 HEADSET STAND"/></a>
                <h4 class="shop-item-title font-alt"><a href="#">HS01 HEADSET STAND</a></h4>Rp441.000
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</section>