<section class="home-section home-fade home-full-height" id="home">
    <div class="hero-slider">
      <ul class="slides">
        <li class="bg-dark-30 bg-dark shop-page-header" style="background-image:url(&quot;assets/images/shop/slider1.jpeg&quot;);">
          <div class="titan-caption">
            <div class="caption-content">
              <a class="section-scroll btn btn-border-w btn-round" href="#latest">Learn More</a>
            </div>
          </div>
        </li>
        <li class="bg-dark-30 bg-dark shop-page-header" style="background-image:url(&quot;assets/images/shop/slider2.png&quot;);">
          <div class="titan-caption">
            <div class="caption-content">
              <a class="section-scroll btn btn-border-w btn-round" href="#latest">Learn More</a>
            </div>
          </div>
        </li>
        <li class="bg-dark-30 bg-dark shop-page-header" style="background-image:url(&quot;assets/images/shop/slider3.png&quot;);">
          <div class="titan-caption">
            <div class="caption-content">
              <div class="font-alt mb-30 titan-title-size-3"> MEG Z490 UNIFY</div>
              <a class="section-scroll btn btn-border-w btn-round" href="#latest">Learn More</a>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </section>