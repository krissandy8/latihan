<div class="module-small bg-dark">
  <div class="container">
  <div class="row">
      <div class="col-sm-3">
      <div class="widget">
          <h5 class="widget-title font-alt">About Comskit</h5>
          <p>Butuh laptop untuk ngerjain tugas, kerja maupun gaming? Comskit menyediakan berbagai laptop, hardware, peralatan komputer untuk berbagai merk dan kebutuhan</p>
          <p>Phone: +62 821 4032 8449</p>
          <a href="https://api.whatsapp.com/send?phone=6282140328449">Klik untuk Chat Whatsapp Toko</a>
      </div>
      </div>
      <div class="col-sm-3">
      <div class="widget">
          <h5 class="widget-title font-alt">Blog Categories</h5>
          <ul class="icon-list">
          <li><a href="#">Motherboards</a></li>
          <li><a href="#">laptops</a></li>
          <li><a href="#">Graphic Cards</a></li>
          <li><a href="#">Monitor</a></li>
          <li><a href="#">Dekstop</a></li>
          </ul>
      </div>
      </div>
      <div class="col-sm-6">
      <div class="widget">
          <h5 class="widget-title font-alt">Find us here</h5>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1067.3756310308825!2d110.36645844874809!3d-7.787302594301049!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a58255e6e1967%3A0xbc37e774b5558b90!2sAudio%20Electronics%20Stores!5e0!3m2!1sen!2sid!4v1610008901753!5m2!1sen!2sid" width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
      </div>
  </div>
  </div>
</div>
<hr class="divider-d">

<footer class="footer bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <p class="copyright font-alt">&copy; 2020&nbsp;<a href="index.html">ComsKit</a>, Ultimate Hardware</p>
        </div>
        <div class="col-sm-6">
          <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>