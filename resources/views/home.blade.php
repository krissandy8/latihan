@extends('layouts.layout')

@section('banner')
@include('layouts.topbanner')
@endsection

@section('navbar')
@include('layouts.navbar')
@endsection

@section('content')
    <section class="module-small">
        <div class="container">
        <div class="row multi-columns-row">
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/MEGX570GODLIKE.png" alt="Accessories Pack"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="/singleprod">MSI MEG X570 GODLIKE</a></h4>RP9.851.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/MEGX570UNIFY.png" alt="MEG X570 UNIFY"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">MSI MEG X570 UNIFY</a></h4>Rp5.728.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/MEGZ390GODLIKE.png" alt="MEG Z390 GODLIKE"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">MSI MEG Z390 GODLIKE</a></h4>Rp10.000.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/Z370GODLIKEGAMING.png" alt="Z370 GODLIKE GAMING"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">MSI Z370 GODLIKE GAMING</a></h4>Rp11.473.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/3090SUPRIMX.png" alt="3090 SUPRIM X"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">RTX 3090 SUPRIM X 24G</a></h4>Rp33.090.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/3080GTRIO.PNG" alt="3080 GAMING TRIO"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">RTX 3090 GAMINB X TRIO</a></h4>Rp31.450.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/3080VENTUS.png" alt="3080 VENTUS"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">RTX 3080 VENTUS 3X 10G</a></h4>Rp18.500.000
            </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                <div class="shop-item-image"><img src="assets/images/shop/3070SUPRIMX.png" alt="3070SUPRIMX"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Add To Cart</span></a></div>
                </div>
                <h4 class="shop-item-title font-alt"><a href="#">RTX 3070 SUPRIM X 8G</a></h4>Rp11.400.000
            </div>
            </div>
        </div>
        <div class="row mt-30">
            <div class="col-sm-12 align-center"><a class="btn btn-b btn-round" href="#">See all products</a></div>
        </div>
        </div>
    </section>
@endsection

@section('exclusive')
@include('layouts.exclusive')
@endsection

@section('footer')
@include('layouts.footer')
@endsection
